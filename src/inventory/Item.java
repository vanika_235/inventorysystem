
package inventory;

/**
 * @author Vanika
 */
public class Item {

    int itemID;
    private String name;
    private int quantity;

  
    public static Item[] inventory = new Item[100];
    private int itemCounter = 0; 
    public Item(int itemID, int quantity, String name) {
        this.itemID = itemID;
        this.quantity = quantity;
        this.name = name;
    }
    public int getItemID()  //getter method for item
    {  
        return itemID;
    }
    
    public int getQuantity()  //getter method for quantity
    {
    return quantity;
}
   public String getName()  //getter method for name
   {
       return name;
   }
   
   public void setItemID(int itemID)  // setter method for itemID
   {
       itemID= itemID;
   }
   
   public void setQuantity(int quantity)   //setter method for quantity
   {
       quantity=quantity;
   }
   
   public void setName(String name)    //setter method for name
   {
       name=name;
   }
 
    public void addItem(Item newItem) {
        inventory[itemCounter] = newItem;
        itemCounter++;
        quantity++;
    }

   
    public void printInventory() {
        for (int i = 0; i < itemCounter; i++) {
            System.out.println("ID: " + inventory[i].itemID
                    + "\t Name: " + inventory[i].name
                    + "\t Quantity:" + inventory[i].quantity);
        }
    }

    
    public int getItemQuantity(int ID) {
        int temp = 0;
        for (int j = 0; j < itemCounter; j++) {
            if (inventory[j].itemID == ID) {
                temp = inventory[j].quantity;
                break;
            }
        }
        return temp;
    }

}
