/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventory;

import java.util.Scanner;

/**
 * @author Vanika
 * @author Vanika
 */
public class InventorySystem {

    public static void main(String[] args) {
      Item item1 = new Item(123, 234, "John");
        Scanner input = new Scanner(System.in);
        System.out.println("Enter name of the item to add:");
       String name = input.nextLine();
        System.out.println("Enter the quantity:");
        int quantity = input.nextInt();
       Item item2 = new Item(++item1.itemID, quantity, name);
        item1.addItem(item2);
        Item item3 = new Item(++item1.itemID, 5, "ET-2750 printer");
        item1.addItem(item3);
        Item item4 = new Item(++item1.itemID, 10, "MX-34 laptops");
        item1.addItem(item4);
        item1.printInventory();
        System.out.println("Enter ID of the item whose quantity you want to find:");
        int temp_ID = input.nextInt();
        System.out.println("Item's quantity is: " + item1.getItemQuantity(temp_ID));

    }

}

